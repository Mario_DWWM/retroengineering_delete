var Model = {
  notes: [],
  addNewNote: function (){
    var newNote = {
      id: Model.notes.length,
      content: ""
    };
    Model.notes.push(newNote);
    View.addNote(newNote);
  },
  closeNote: function(id, text){
    const findId = (elmt) => id == elmt.id;
    let noteId = Model.notes.findIndex(findId);
    Model.notes[noteId].content = text;
    View.disableNote(Model.notes[noteId]);
  },
  deleteNote: (id) => {
    View.removeNote(id);
    Model.notes.splice(id, 1);
  }
};
